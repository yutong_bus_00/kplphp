<?php
/**
 * Created by PhpStorm.
 * User: Win10 - Jeffery 13..@qq.com
 * Date: 2019/11/16
 * Time: 14:40
 */
namespace addons\kuaidi\controller;
use addons\BaseController;

use addons\kuaidi\model\KuaidiConfig;


class Admin extends BaseController
{
    public function initialize()
    {
        parent::initialize();
    }

    # 管理入口
    public function manage()
    {
        if(request()->isPost())
        {
            $KuaidiConfig = new KuaidiConfig();
            $data = request()->post();
            $newdata = [];
            foreach ($data['customer'] as $k => $v)
            {
                $newdata[$k]['id'] = $k;
                $newdata[$k]['customer'] = $v;
                $newdata[$k]['key'] = $data['key'][$k];
            }
            $KuaidiConfig->saveAll($newdata);
            $this->success('保存成功');
        }
        $list = KuaidiConfig::select();
        return view('../addons/kuaidi/view/admin/manage.html', ['list'=>$list]);
    }


    public function link()
    {
        echo '试试访问该插件这个link的东西';
    }
}